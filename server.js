const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')


// Routes
const users = require('./routes/api/users')
const profile = require('./routes/api/profile')
const posts = require('./routes/api/posts')

// DB
const db = require('./config/keys').mongoURI

// Connect to mongo
mongoose.connect(db)
    .then(() => console.log('MongDB Connected'))
    .catch(err => console.log(err))

// Body-Parser Middleware
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// Passport Middleware
app.use(passport.initialize())
// Passpost Config
require('./config/passport')(passport)

// Get route
app.use('/api/users', users)
app.use('/api/profile', profile)
app.use('/api/posts', posts)

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server running on port ${port}`))