import React, { Component } from 'react';
import './App.css';
import Navbar from './components/layout/Navbar'
import Footer from './components/layout/Footer'
import Landing from './components/layout/Landing'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Register from './components/auth/Register'
import Login from './components/auth/Login'
import { Provider } from 'react-redux'
import store from './store'
import jwt_decode from 'jwt-decode'
import setAuthToken  from './utils/setAuthToken'
import { setCurrentUser, logoutUser } from './actions/authActions'
import Dashboard from './components/dashboard/dashboard'
import { clearProfile } from './actions/profileActions'

// Check for token
if(localStorage.jwtToken){
  // set auth token header auth
  setAuthToken(localStorage.jwtToken) 
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken)
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded))
  // Check for expired token
  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime){
    // Logout user
    store.dispatch(logoutUser())
    // clear user
    store.dispatch(clearProfile())
    // TODO: Clear current Profile
    // Redirect to login
    window.location.href = '/login'
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Navbar/>
              <Route exact path="/" component={Landing}/>
              <div className="container">
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/dasboard" component={Dashboard}/>
              </div>
            <Footer/>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
